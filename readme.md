# InterfaceOptionsExtended #

This is an alternative to lib_InterfaceOptions which allows character-specific saving of Addon settings.
It also comes with a profile manager, so users can copy their settings from one character to another, and even create their own profiles and switch between them.

## About ##

With the introduction of multiple characters per account it seemed reasonable to offer an option for addon authors to easily implement character-specific settings.

This is a minor inconvenience for general settings like layout, but very useful when it comes to saving character specific data like inventory items or custom lists.

Settings are saved per character by default to provide a reliable catchall solution, however feel free to adjust the settings saving behaviour according to your users needs. 

By using the Profile Selector you can offer an easy way for your users to manage their character-specific settings, or even create new profiles.
  
## Usage ##

Use just like `lib_InterfaceOptions`.

All of the following settings are saved PER CHARACTER by default now:

    AddCheckBox
    AddSlider
    AddTextInput
    AddColorPicker
    AddChoiceMenu
    AddChoiceEntry
    StartGroup
    AddMovableFrame
    
To explicitly save a setting for ALL characters, the syntax is:

    InterfaceOptions.Add____(options, true);

Manually saved data via `Component.SaveSetting("name", values)` are also PER CHARACTER by default.
As above, to explicitly make the setting the same for all character, use:

    Component.SaveSetting("name", values, true)

Reading a manual setting does not require any flags to specify it's origin, and is the same as before:

	Component.GetSetting("perCharacterSetting")
	Component.GetSetting("GlobalSetting")

### Profile Manager ###

`InterfaceOptions.AddProfileSelector()` adds a new OptionGroup (no nesting!) that allows users to copy their settings from one character to another.
This MUST be called AFTER `OnComponentLoad` fired, or it will break.
To copy settings, simply select the alternate profile and hit the button.

The Profile Manager also allows creating new profiles, and freely switching or copying settings between them.

In addition, users are able to export their current settings profile to share or restore it as needed. 

## Examples ##

    require "./lib/lib_InterfaceOptionsExtended";

Interface Options are saved per character by default, no need to do anything different

    InterfaceOptions.AddCheckBox({id="checkbox_per_character", label="checkbox_per_character", checkbox=true, default=true});
  
If you want to explicitly save an option on a global basis so it is the same for every character, the syntax changes to: `InterfaceOptions.Add______(optionsTable, true);`

	InterfaceOptions.AddCheckBox({id="checkbox_all_characters", label="checkbox_all_characters", checkbox=true, default=true}, true);

The same changes apply to frames, they are saved per character by default, and global when you use `.AddMovableFrame(options, true)` 

    InterfaceOptions.AddMovableFrame({
      frame = PCFRAME,
      label = "Per Character Frame",
      scalable = true,
    });

    InterfaceOptions.AddMovableFrame({
      frame = ACFRAME,
      label = "All Character Frame",
      scalable = true,
    }, true);

This also applies to manual settings saving - check ui_savedsettings file to compare. 

    Component.SaveSetting("perCharacterTime", System.GetLocalUnixTime())
    Component.SaveSetting("allCharactersTime", System.GetLocalUnixTime(), true)
  
When loading settings, both options are checked, no need to specify global behavior. 

    log("perCharacterTime " .. tostring(Component.GetSetting("perCharacterTime")))
    log("allCharactersTime " .. tostring(Component.GetSetting("allCharactersTime")))

To attach the Profile Selector, `InterfaceOptions.AddProfileSelector()` must be called AFTER `OnComponentLoad`, or it will break!
Since the Profile Selector adds an option group, nesting it between `InterfaceOptions.StartGroup()` and `.StopGroup()` is not possible!

    function OnComponentLoad() 

      InterfaceOptions.SetCallbackFunc(function(id, val)  OnMessage({type=id, data=val})  end, ADDON.NAME)
      InterfaceOptions.AddProfileSelector();

    end

## Issue Tracking ##

Please report any issues you might find [here](https://bitbucket.org/Jotaro__/lib_interfaceoptionsextended/issues)
    
## Changelog ##

    1.0.0 - initial release
  
      # Features
      - Save all InterfaceOptions settings on a per-character basis by default
      - Option to save specific settings globally
      - Profile Manager to easily change and edit existing profiles,
          including custom ones.
      - Profiles can also be exported and imported
