----
--  lib_InterfaceOptionsExtended
--  @Author ~%author%~
--  @Version ~%version%~
--

--[===[
  
  Intent:
  
    With the introduction of multiple characters per account it seemed
    reasonable to offer an option for addon authors to
    easily implement character-specific settings.
    
    This is a minor inconvenience for general settings like layout,
    but very useful when it comes to saving character specific data like 
    inventory items or custom lists.
    
    Settings are saved per character by default to provide a reliable catchall solution,
    however feel free to adjust the settings saving behaviour according to your users needs. 
    
    By using the Profile Selector you can offer an easy way for your users to manage their character-specific settings,
    or even create new profiles. 
    
  Usage:
    
    Use just like lib_InterfaceOptions.
    
    All of the following settings are saved PER CHARACTER by default now:
        AddCheckBox, AddSlider, AddTextInput, AddColorPicker, AddChoiceMenu, AddChoiceEntry, StartGroup, AddMovableFrame
        
    To explicitly save a setting for ALL characters, the syntax is: InterfaceOptions.Add____(options, true);
    
    Manually saved data via Component.SaveSetting("name", values) are also PER CHARACTER by default.
    As above, to explicitly make the setting the same for all character, use:  Component.SaveSetting("name", values, true)
    
    Reading a manual setting does not require any flags to specify it's origin, and is the same as before:
    Component.GetSetting("perCharacterSetting")
    Component.GetSetting("GlobalSetting")
    
    
  Profile Copy Assist:
  
    InterfaceOptions.AddProfileSelector() adds a new OptionGroup (no nesting!) that allows users to copy their settings from one character to another.
    This MUST be called AFTER OnComponentLoad fired, or it will break.
    To copy settings, simply select the alternate character and hit the button.
  
  
  
--]===]

require "string";
require "lib/lib_table";
require "lib/lib_InterfaceOptions";

--==================--
-- Constants        --
--==================--

local c_version = "~%version%~";

local c_hasGlobal = {
  AddCheckBox = true,
  AddSlider = true,
  AddTextInput = true,
  AddColorPicker = true,
  AddChoiceMenu = true,
  AddChoiceEntry = true,
  StartGroup = true,
  --AddMovableFrame = true, -- handled seperately
};

local c_alwaysGlobal = {
  _Options_Version = true,
  _Options_Profiles = true,
}

local c_internalCallbacks = {
  _options_profile_group = true, 
  _options_profile_choice = true, 
  _options_profile_load = true,
  _options_profile_copy = true,
  _options_profile_delete = true,
  _options_profile_create_name = true,
  _options_profile_create = true,
  _options_profile_import = true,
  _options_profile_import_value = true,
  _options_profile_export = true,
  _options_profile_import_character = true,
  _options_profile_import_global = true
}

--==================--
-- Variables        --
--==================--

local lf = {}; -- table for local functions
local f_callback = nil;
local lng = {};

local g_InterfaceOptionEntries = {}; -- all registered entries
local g_saveOptionGlobal = {}; -- save those global regardless of character


local g_playerId = nil;
local g_registeredProfiles = {};

local g_selectedProfile = nil;
local g_choiceProfile = nil;

local g_newProfileName = "";

local g_importProfileTable = {};
local g_importProfileValid = false;
local g_importProfileParts = {
  _character = true,
  _global = true
}

--==================--
-- InterfaceOptions --
--==================--

local InterfaceOptionsOriginal = _table.copy(InterfaceOptions);

local InterfaceOptionsL = {};
local META_INTERFACE = {
  __index = function(t,key) return InterfaceOptionsOriginal[key]; end
};
setmetatable(InterfaceOptionsL, META_INTERFACE);

InterfaceOptionsL.isExtended = true;

for k, v in pairs(c_hasGlobal) do
  InterfaceOptionsL[k] = function(option, global)
    lf.checkAndAddGlobal(k, option, global);
  end
end

InterfaceOptionsL.AddMovableFrame = function(option, global)
  option.id = lf.GetFrameName(option.frame);
  lf.checkAndAddGlobal("AddMovableFrame", option, global);
end

InterfaceOptionsL.SetCallbackFunc = function(func, title)
  f_callback = func;
  InterfaceOptionsOriginal.SetCallbackFunc(lf.profileCallback, title);
end

InterfaceOptionsL.AddProfileSelector = function()
  assert(System and Component and Player and Game, "Cannot attach Profile Selector at this time! Run AFTER OnComponentLoaD has fired.");

  local locale = System.GetLocale();
  g_registeredProfiles = Component.GetSetting("_Options_Profiles") or {};
  g_registeredProfiles[Player.GetCharacterId()] = Game.GetTargetInfo(Player.GetTargetId()).name or g_registeredProfiles[Player.GetCharacterId()];
  Component.SaveSetting("_Options_Profiles", g_registeredProfiles);

  
  local currentProfileText = g_registeredProfiles[g_selectedProfile];
  if(not currentProfileText or type(currentProfileText) == "boolean")then
      currentProfileText = lng[locale].PROFILE_NEW;      
  end
  
  InterfaceOptionsOriginal.StartGroup({id="_options_profile_group", label=lng[locale].PROFILE_SELECTOR.." - "..currentProfileText, checkbox=true, default=false, tooltip=lng[locale].PROFILE_SELECTOR_TOOLTIP});
    
    InterfaceOptionsOriginal.AddChoiceMenu({id="_options_profile_choice", label=lng[locale].PROFILE_CHOICE, default=(g_selectedProfile or "_default"), tooltip=lng[locale].PROFILE_CHOICE_TOOLTIP})
  
    InterfaceOptionsOriginal.AddChoiceEntry({menuId="_options_profile_choice", label=lng[locale].PROFILE_CHOICE_DEFAULT, val="_default"})
    
    for k, v in lf.pairsByKeys(g_registeredProfiles)do
      local label = v;
      if(type(v) == "boolean")then
        label = lng[locale].PROFILE_NEW;      
      end
      InterfaceOptionsOriginal.AddChoiceEntry({menuId="_options_profile_choice", label=label, val=k})
    end
 
   
    InterfaceOptionsOriginal.AddButton({id="_options_profile_load", label=lng[locale].PROFILE_LOAD, tooltip=lng[locale].PROFILE_LOAD_TOOLTIP})
    InterfaceOptionsOriginal.AddButton({id="_options_profile_copy", label=lng[locale].PROFILE_COPY, tooltip=lng[locale].PROFILE_COPY_TOOLTIP})
    InterfaceOptionsOriginal.AddButton({id="_options_profile_delete", label=lng[locale].PROFILE_DELETE, tooltip=lng[locale].PROFILE_DELETE_TOOLTIP})
  
    -- --------------------------------
    InterfaceOptionsOriginal.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=1200, padding=12, tint="#505050"})
    -- --------------------------------
    InterfaceOptionsOriginal.AddTextInput({id="_options_profile_create_name", default=g_newProfileName, label=lng[locale].PROFILE_CREATE_NAME, tooltip=lng[locale].PROFILE_CREATE_NAME_TOOLTIP});
    InterfaceOptionsOriginal.AddButton({id="_options_profile_create", label=lng[locale].PROFILE_CREATE, tooltip=lng[locale].PROFILE_CREATE_TOOLTIP})
    
    -- --------------------------------
    InterfaceOptionsOriginal.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=1200, padding=12, tint="#505050"})
    -- --------------------------------
    
    InterfaceOptionsOriginal.AddButton({id="_options_profile_export", label=lng[locale].PROFILE_EXPORT, tooltip=lng[locale].PROFILE_EXPORT_TOOLTIP})
    InterfaceOptionsOriginal.AddTextInput({id="_options_profile_import_value", default="", maxlen=-1, label=lng[locale].PROFILE_IMPORT_VALUE, tooltip=lng[locale].PROFILE_IMPORT_VALUE_TOOLTIP});
    InterfaceOptionsOriginal.AddCheckBox({id="_options_profile_import_character", checkbox=true, default=g_importProfileParts._character, label=lng[locale].PROFILE_IMPORT_CHARACTER, tooltip=lng[locale].PROFILE_IMPORT_CHARACTER_TOOLTIP});
    InterfaceOptionsOriginal.AddCheckBox({id="_options_profile_import_global", checkbox=true, default=g_importProfileParts._global, label=lng[locale].PROFILE_IMPORT_GLOBAL, tooltip=lng[locale].PROFILE_IMPORT_GLOBAL_TOOLTIP});
    InterfaceOptionsOriginal.AddButton({id="_options_profile_import", label=lng[locale].PROFILE_IMPORT, tooltip=lng[locale].PROFILE_IMPORT_TOOLTIP}) 
      
    
  InterfaceOptions.StopGroup()
end


InterfaceOptions = InterfaceOptionsL;

--==================--
-- Component        --
--==================--

local ComponentOriginal = _table.copy(Component);
local ComponentL = {};
local META_COMPONENT = {
  __index = function(t,key) return ComponentOriginal[key]; end
};
setmetatable(ComponentL, META_COMPONENT);

ComponentL.isExtended = true;

ComponentL.SaveSetting = function(key, data, global)

  if(global)then
    g_saveOptionGlobal[key] = true;
  end
  
  -- seems optiongroups and select entries get piped here anyway, so grab them again
  if(c_internalCallbacks[string.match(key, ":(.+)$")])then
    return
  end
    
  
  local wrap = lf.wrapSettingInProfile(key, data);
  ComponentOriginal.SaveSetting(wrap.key, wrap.data);
  
end

ComponentL.GetSetting = function(key)
  return lf.unwrapSettingFromProfile(key);
end


Component = ComponentL;

--==================--
-- Local Functions  --
--==================--

lf.warnOptionIdReserved = function(id)
  local warning = false;
  if(string.match(tostring(id), "^_options_profile_.+"))then
      warning = true;
  end
  if(warning)then
    warn(tostring(id).." falls under the Profile Selector naming scheme, to ensure future compatibility using it is discouraged.")
  end
end

lf.checkAndAddGlobal = function(key, option, global)
  if(option.id)then
    
    assert((not c_internalCallbacks[option.id]), tostring(option.id).." is reserved for Profile Selector");
    lf.warnOptionIdReserved(option.id)
  
    g_InterfaceOptionEntries[option.id] = option;
    if(global)then
      g_saveOptionGlobal[option.id] = option;
    end
  end
  InterfaceOptionsOriginal[key](option);
end


lf.wrapSettingInProfile = function(key, data, overrideProfile)
  
  g_playerId = g_playerId or Player.GetCharacterId();
  g_registeredProfiles[g_playerId] = g_registeredProfiles[g_playerId] or true;
  g_selectedProfile = g_selectedProfile or g_playerId;
  
  if(Component.GetSetting(g_playerId))then
    g_selectedProfile = Component.GetSetting(g_playerId)._options_profile_choice or g_selectedProfile 
  end
  
  local loadFrom = g_selectedProfile
  
  local res = {key = key, data = data}
  
  if(
    (not c_alwaysGlobal[key]) and
    (not g_saveOptionGlobal[key]) and
    (not g_saveOptionGlobal[string.match(key, ":(.+)$")])
  )then
    
    
    local settingsWrapper = ComponentOriginal.GetSetting(overrideProfile or loadFrom) or {};
    settingsWrapper[string.lower(key)] = data;
    
    res = {key = overrideProfile or loadFrom, data = settingsWrapper}
    
  end
  
  return res;
  
end

lf.unwrapSettingFromProfile = function(key, overrideProfile)
  
  local res = nil;
  
  local profile = overrideProfile or g_selectedProfile;
  
  res = ComponentOriginal.GetSetting(profile or "")
  
  if(res)then
    res = res[string.lower(key)];
  end
  
  if(not res)then
    res = ComponentOriginal.GetSetting(key)
  end
  
  return res;

  
end

lf.copySettings = function(profile)
  for k, v in pairs(g_InterfaceOptionEntries)do
    Component.SaveSetting(lf.spoofKey(k, v), lf.unwrapSettingFromProfile(lf.spoofKey(k, v),profile))
  end
end

lf.importSettings = function()

  for k, v in pairs(g_InterfaceOptionEntries)do
    
    local key = lf.spoofKey(k, v);
    local isGlobal = false;
    
    if(g_saveOptionGlobal[k] or c_alwaysGlobal[k])then
       isGlobal = true; 
    end
    if(isGlobal)then
      if(g_importProfileParts._global)then
        Component.SaveSetting(lf.spoofKey(k, v), g_importProfileTable._global[key])
      end
    else
      if(g_importProfileParts._character)then
        Component.SaveSetting(lf.spoofKey(k, v), g_importProfileTable._character[key])
      end
    end
  end
end

lf.sanitizeName = function(str)
  str = string.lower(str);
  str=string.gsub(str, "^%s+", "");
  str=string.gsub(str, "%s+$", "");
  str=string.gsub(str, "%s", "_");
  str=string.gsub(str, "[^%w_]", "");
  return str
end

lf.spoofKey = function(key, setting)
  local spoofed = key;
  if(type(setting) == "table" and setting.type and setting.id)then
    spoofed = string.lower("option-"..setting.type..":"..setting.id);
  end
  return spoofed;
end

lf.systemMessage = function(key)
  local locale = System.GetLocale();
  Component.GenerateEvent("MY_SYSTEM_MESSAGE", {text=lng[locale].MESSAGE_PREFIX..lng[locale][key]})
end

-- http://www.lua.org/pil/19.3.html
lf.pairsByKeys = function(t, f)
  local a = {}
  for n in pairs(t) do table.insert(a, n) end
  table.sort(a, f)
  local i = 0      -- iterator variable
  local iter = function ()   -- iterator function
    i = i + 1
    if a[i] == nil then return nil
    else return a[i], t[a[i]]
    end
  end
  return iter
end

lf.generateExportString = function()

    local res = {_character = ComponentOriginal.GetSetting(g_selectedProfile), _global = {}};
    for k, v in pairs(g_saveOptionGlobal) do
      res._global[lf.spoofKey(k, v)] = ComponentOriginal.GetSetting(lf.spoofKey(k, v));
    end
    
    res._character._options_profile_choice = nil;
    res = tostring(res);
    res = string.gsub(res, "\r", "")
    res = string.gsub(res, "\n", "")
    res = string.gsub(res, "\t", "")

    return res;
end

lf.validateImportString = function(settingsString)
  
  g_importProfileValid = false;
  local settingsTable = jsontotable(settingsString);
    
  g_importProfileValid = (settingsTable and settingsTable._character and settingsTable._global);
  
  if(g_importProfileValid)then
    g_importProfileTable = settingsTable;
  end
  
  return g_importProfileValid;
  
end

lf.profileCallback = function(id, val)
  
  if(c_internalCallbacks[id])then
    
    
    local importButtonDisabled = function()
      return (not g_importProfileValid or (not g_importProfileParts._character and not g_importProfileParts._global));
    end
    
    -- profile selection and copy
    if(id == "_options_profile_choice")then
      g_choiceProfile = val;
      local disabled = (val == "_default") or (val == tostring(g_selectedProfile))
      InterfaceOptions.DisableOption("_options_profile_copy", disabled)
      InterfaceOptions.DisableOption("_options_profile_load", disabled)
      InterfaceOptions.DisableOption("_options_profile_delete", (val == "_default"))
    elseif(id == "_options_profile_load")then
      local wrap = lf.wrapSettingInProfile("_options_profile_choice", g_choiceProfile, g_playerId);
      ComponentOriginal.SaveSetting(wrap.key, wrap.data);
      g_selectedProfile = g_choiceProfile;
      System.ReloadUI();
    elseif(id == "_options_profile_copy")then
      lf.copySettings(g_choiceProfile);
      System.ReloadUI();
      
    --profile deletion
    elseif(id == "_options_profile_delete")then  
      g_registeredProfiles[g_choiceProfile] = nil;
      Component.SaveSetting("_Options_Profiles", g_registeredProfiles);
      if(g_choiceProfile == g_selectedProfile)then
        g_choiceProfile = g_playerId;
        ComponentOriginal.SaveSetting(g_playerId, nil);
      else
        ComponentOriginal.SaveSetting(g_choiceProfile, nil);
        g_choiceProfile = g_selectedProfile;
      end
      lf.profileCallback("_options_profile_load", nil);
    
    --profile creation
    elseif(id == "_options_profile_create_name")then  
      g_newProfileName = val;
      InterfaceOptions.DisableOption("_options_profile_create", (#lf.sanitizeName(g_newProfileName) == 0))
    elseif(id == "_options_profile_create")then  
      
      local sName = lf.sanitizeName(g_newProfileName);
     
      if(#sName == 0)then
        warn("sanitized name is too short");
        lf.systemMessage("MESSAGE_CREATE_INVALID")
        return;
      end
      
      if(g_registeredProfiles[sName])then
        warn("profile name already exists");
        lf.systemMessage("MESSAGE_CREATE_EXISTS")
        return;
      end
          
      g_registeredProfiles[sName] = g_newProfileName;
      InterfaceOptions.AddChoiceEntry({menuId="_options_profile_choice", label=g_newProfileName, val=sName})
      Component.SaveSetting("_Options_Profiles", g_registeredProfiles);
      g_choiceProfile = sName;
      lf.profileCallback("_options_profile_load", nil);
      
    --profile export import
    elseif(id == "_options_profile_import_value")then
      InterfaceOptions.DisableOption("_options_profile_import", not lf.validateImportString(val))
    elseif(id == "_options_profile_import_character")then
      g_importProfileParts._character = val;
      InterfaceOptions.DisableOption("_options_profile_import", importButtonDisabled())
    elseif(id == "_options_profile_import_global")then
      g_importProfileParts._global = val;
      InterfaceOptions.DisableOption("_options_profile_import", importButtonDisabled())
    elseif(id == "_options_profile_import")then
      lf.importSettings();
      System.ReloadUI();   
    elseif(id == "_options_profile_export")then
      lf.showCopyBox(lf.generateExportString());
    end
    
    return;
  end
  
  f_callback(id, val);
  
end

lf.showCopyBox = function(text)
  
  local locale = System.GetLocale();  

  local w_copybox = [[
    <Border dimensions="dock:fill" class="PanelBackDrop" style="clip-children:true">
    </Border>
  ]]
  
  local w_focus = [[
    <FocusBox name="CopyBG" dimensions="dock:fill">
      <StillArt name="bg" dimensions="dock:fill" style="texture:colors; tint:#000000; region:white; alpha:0.8"/>
    </FocusBox>
  ]]
  
  local w_desc = [[
    <Text name="text" dimensions="dock:fill;" style="halign:center; valign:center; text-color:#DADADA"/>
  ]]
  
  local w_text = [[
    <TextInput name="TextInput" dimensions="dock:fill" style="font:UbuntuRegular_10;
        valign:top; halign:left; texture:colors; region:transparent; tint:FF000000;
        text-color:#B5B5B5; padding:0; multiline:true; wrap:true;">
    </TextInput>
  ]]
  
  
  local FRAME = Component.CreateFrame("PanelFrame");
  FRAME:SetDims("top:0%; left:0%; right:100%; bottom:100%;")
  FRAME:SetDepth(-10)
  Component.SetInputMode("cursor") --leave in just in case
  FRAME:Show(true)
  
  local BACKGROUND = Component.CreateWidget(w_focus, FRAME)
  
  local COPYBOX = Component.CreateWidget(w_copybox, FRAME);
  COPYBOX:Show(true)
  COPYBOX:SetDims("top:50%-50; left:50%-150; width:300; height:100;")
  
  local DESCRIPTION = Component.CreateWidget(w_desc, FRAME);
  DESCRIPTION:SetDims("top:50%-80; left:50%-150; width:300; height:30;")
  DESCRIPTION:SetText(lng[locale].TEXT_EXPORT_DESCRIPTION);
  
  local TEXT = Component.CreateWidget(w_text, COPYBOX);
  TEXT:SetText(text);
  TEXT:SetFocus();
  
  local function destroy()
    Component.RemoveFrame(FRAME)
  end
  
  BACKGROUND:BindEvent("OnMouseDown", function()
    destroy()
  end);
  BACKGROUND:BindEvent("OnRightMouse", function()
    destroy()
  end);
  FRAME:BindEvent("OnEscape", function()
    destroy()
  end)

  
end

-- from lib_InterfaceOptions
lf.GetFrameName = function(frame)
  local name, FRAME
  if type(frame) == "string" then
    name = frame
    FRAME = Component.GetFrame(frame)
  else
    name = frame:GetInfo()
    FRAME = frame
  end
  return name, FRAME
end

--==================--
-- Language         --
--==================--

lng.en = {
  PROFILE_NEW = "(new)",
  PROFILE_SELECTOR = "Profile Manager",
  PROFILE_SELECTOR_TOOLTIP = "Show profile selector\n(version "..c_version..")",
  PROFILE_CHOICE = "Profile (Buttons will trigger /rui!)",
  PROFILE_CHOICE_TOOLTIP = "Which profile to load or copy from",
  PROFILE_CHOICE_DEFAULT = "(Select profile)",
  PROFILE_LOAD = "Load selected profile",
  PROFILE_LOAD_TOOLTIP = "Switch to the the selected profile",
  PROFILE_COPY = "Copy from selected profile",
  PROFILE_COPY_TOOLTIP = "Copy the settings into your currently selected profile",
  PROFILE_DELETE = "DELETE selected profile",
  PROFILE_DELETE_TOOLTIP = "Remove the selected profile and switch back to the characters default one if needed",
  PROFILE_CREATE_NAME = "Create new profile",
  PROFILE_CREATE_NAME_TOOLTIP = "Name of the new profile",
  
  PROFILE_CREATE = "Save new profile",
  PROFILE_CREATE_TOOLTIP = "Create and switch to the new profile",
  
  PROFILE_IMPORT_VALUE = "Import settings into current profile",
  PROFILE_IMPORT_VALUE_TOOLTIP = "Paste an export object to OVERRIDE your current settings with the exported version",
  PROFILE_IMPORT = "Import settings from export object",
  PROFILE_IMPORT_TOOLTIP = "OVERRIDE your current settings with the import values",
  
  PROFILE_IMPORT_CHARACTER = "Import character settings",
  PROFILE_IMPORT_CHARACTER_TOOLTIP = "Import all character-specific settings from the provided export object",
  PROFILE_IMPORT_GLOBAL = "Import global settings",
  PROFILE_IMPORT_GLOBAL_TOOLTIP = "Import all shared settings from the provided export object",
  
  PROFILE_EXPORT = "Export settings to text object",
  PROFILE_EXPORT_TOOLTIP = "Export your current settings so they can be restored at a later date",
  
  MESSAGE_PREFIX = "Profile Manager ("..c_version.."): ",
  MESSAGE_CREATE_INVALID = "Provided profile identifier is invalid!",
  MESSAGE_CREATE_EXISTS = "Provided profile identifier already exists!",
  
  TEXT_EXPORT_DESCRIPTION = "Use ctrl-a, ctrl-c to copy the current settings"
}
lng.de = {
  PROFILE_NEW = "(neu)",
  PROFILE_SELECTOR = "Profilmanager",
  PROFILE_SELECTOR_TOOLTIP = "Profilmanager anzeigen\n(Version "..c_version..")",
  PROFILE_CHOICE = "Profil (Buttons lösen /rui aus!)",
  PROFILE_CHOICE_TOOLTIP = "Welches Profil geladen oder kopiert werden soll",
  PROFILE_CHOICE_DEFAULT = "(Profil wählen)",
  PROFILE_LOAD = "Gewähltes Profil laden",
  PROFILE_LOAD_TOOLTIP = "Zu gewähltem Profil wechseln",
  PROFILE_COPY = "Von gewähltem Profil kopieren",
  PROFILE_COPY_TOOLTIP = "Kopiert alle Einstellungen des gewählten Profils in das aktuelle",
  PROFILE_DELETE = "Gewähltes Profil LÖSCHEN",
  PROFILE_DELETE_TOOLTIP = "Entfernt das gewählte Profil und wechselt zum Standard-Profil, falls nötig",
  PROFILE_CREATE_NAME = "Neues Profil erstellen",
  PROFILE_CREATE_NAME_TOOLTIP = "Name des neuen Profils",
  
  PROFILE_CREATE = "Neues Profil speichern",
  PROFILE_CREATE_TOOLTIP = "Erstelle und wechsle zu dem neuen Profil",
  
  PROFILE_IMPORT_VALUE = "Einstellungen importieren",
  PROFILE_IMPORT_VALUE_TOOLTIP = "Exportobjekt einfügen, welches das aktuelle Profil ÜBERSCHREIBEN soll",
  PROFILE_IMPORT = "Einstellungen aus Objekt importieren",
  PROFILE_IMPORT_TOOLTIP = "ÜBERSCHREIBT das aktuelle Profil mit den Einstellungen aus dem Exportobjekt",
  
  PROFILE_IMPORT_CHARACTER = "Importiere Charaktereinstellungen",
  PROFILE_IMPORT_CHARACTER_TOOLTIP = "Importiert alle characterspezifischen Einstellungen",
  PROFILE_IMPORT_GLOBAL = "Importiere globale Einstellungen",
  PROFILE_IMPORT_GLOBAL_TOOLTIP = "Importiert alle gemeinsamen Einstellungen",
  
  PROFILE_EXPORT = "Einstellungen exportieren",
  PROFILE_EXPORT_TOOLTIP = "Exportiert alle aktuellen Einstellungen, um sie später wiederherzustellen",
  
  MESSAGE_PREFIX = "Profilmanager ("..c_version.."): ",
  MESSAGE_CREATE_INVALID = "Angegebener Profilname ist ungültig!",
  MESSAGE_CREATE_EXISTS = "Angegebener Profilname existiert bereits",
  
  TEXT_EXPORT_DESCRIPTION = "Nutze Ctrl-A und Ctrl-C um zu kopieren"
}
lng.fr = {}
lng.cn = {}

local META_LNG = {
  __index = function(t,key) return lng.en[key]; end
};
setmetatable(lng.de, META_LNG);
setmetatable(lng.fr, META_LNG);
setmetatable(lng.cn, META_LNG);