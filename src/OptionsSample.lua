----
--  OptionsSample
--  @Description ~%description%~
--  @Author ~%author%~
--  @Version ~%version%~
--  @Patch ~%patch%~
--

require "lib/lib_Debug";

--[[
  load this instead of "lib/lib_InterfaceOptions"
]]--
require "./lib/lib_InterfaceOptionsExtended";


--~~~~~~~~~~~~~--
-- Constants   --
--~~~~~~~~~~~~~--

local PCFRAME = Component.GetFrame("PerCharacterFrame");
local PCTEXT = Component.GetWidget("charactertext");
local ACFRAME = Component.GetFrame("AllCharactersFrame");
local ACTEXT = Component.GetWidget("alltext");


--~~~~~~~~~~~~~--
-- Options     --
--~~~~~~~~~~~~~--

local ADDON = {
  NAME = "OptionsSample",
  VERSION = "~%version%~",
  AUTHOR = "~%author%~",
  LOADED = false,
  DEBUG = true
}


InterfaceOptions.SaveVersion(1)
InterfaceOptions.StartGroup({id="enabled", label="Enable "..ADDON.NAME, checkbox=true, default=true, tooltip="Disables the Addon if unchecked"});
  

  --[[
    Interface Options are saved per character by default, no need to do anything different
  ]]--
  
  InterfaceOptions.AddCheckBox({id="checkbox_per_character", label="checkbox_per_character", checkbox=true, default=true});
  InterfaceOptions.AddSlider({id="slider_per_character", label="slider_per_character", default=50, min=0, max=100, inc=5});
  InterfaceOptions.AddTextInput({id="text_per_character", label="text_per_character", default=""});
  InterfaceOptions.AddColorPicker({id="color_per_character", label="color_per_character", default={tint="ff0000", alpha=0.5}})


  -- --------------------------------
  -- this is just a seperator 
  InterfaceOptions.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=600, padding=12, tint="#505050"})
  -- -------------------------------- 
  
  --[[
    If you want to explicitly save an option on a global basis
    so it is the same for every character, the syntax changes to:
    InterfaceOptions.Add______(optionsTable, true);
  ]]--

  InterfaceOptions.AddCheckBox({id="checkbox_all_characters", label="checkbox_all_characters", checkbox=true, default=true}, true);
  InterfaceOptions.AddSlider({id="slider_all_characters", label="slider_all_characters", default=50, min=0, max=100, inc=5}, true);
  InterfaceOptions.AddTextInput({id="text_all_characters", label="text_all_characters", default=""}, true);
  InterfaceOptions.AddColorPicker({id="color_all_characters", label="color_all_characters", default={tint="ff0000", alpha=0.5}}, true)
  
InterfaceOptions.StopGroup()

--[[
  Same changes to frames, they are saved per character by default, and global when you use .AddMovableFrame(options, true) 
]]--
InterfaceOptions.AddMovableFrame({
  frame = PCFRAME,
  label = "Per Character Frame",
  scalable = true,
});

InterfaceOptions.AddMovableFrame({
  frame = ACFRAME,
  label = "All Character Frame",
  scalable = true,
}, true);





InterfaceOptions.NotifyOnLoaded(true)

function OnMessage(msg)
  if msg.type == "__LOADED" then
    ADDON.LOADED = true
    init();
  end
  log("Setting changed: "..tostring(msg))
end


--~~~~~~~~~~~~~--
-- Events      --
--~~~~~~~~~~~~~--

function OnComponentLoad() 

  log(ADDON.NAME..": --- version " .. ADDON.VERSION ..  " ---")
  Debug.EnableLogging(ADDON.DEBUG);
    
  PCTEXT:SetText("This frame will save per character");
  PCTEXT:SetAlignment("halign", "center")
  ACTEXT:SetText("This frame will be at the same position for all characters");
  ACTEXT:SetAlignment("halign", "center")
  
  InterfaceOptions.SetCallbackFunc(function(id, val)  OnMessage({type=id, data=val})  end, ADDON.NAME.." "..ADDON.VERSION)
  --[[
    To attach the Profile Selector, this must be called AFTER OnComponentLoad, or it will break!
    Since the Profile Selector adds an option group, nesting it between InterfaceOptions.StartGroup() and .StopGroup() is not possible!
  ]]--
  InterfaceOptions.AddProfileSelector();
  
end

--~~~~~~~~~~~~~--
-- Functions   --
--~~~~~~~~~~~~~--

function init()
    systemMessage(ADDON.NAME.." "..ADDON.VERSION..' loaded!')
    
    readThisLater();
    
  --[[
    This also applies to manual settings saving - check ui_savedsettings file to compare. 
  ]]--
  Component.SaveSetting("perCharacterTime", System.GetLocalUnixTime())
  Component.SaveSetting("allCharactersTime", System.GetLocalUnixTime(), true)
  
  
    
end

function readThisLater()
  --[[
    When loading settings, both options are checked, no need to specify global behavior. 
  ]]--
  log("perCharacterTime " .. tostring(Component.GetSetting("perCharacterTime")))
  log("allCharactersTime " .. tostring(Component.GetSetting("allCharactersTime")))
end

function systemMessage(args)
  Component.GenerateEvent("MY_SYSTEM_MESSAGE", {text=tostring(args)})
end




