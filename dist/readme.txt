[quote][SIZE=6][COLOR=#ffcc33]InterfaceOptionsExtended[/COLOR][/SIZE][/quote]

This is an alternative to lib_InterfaceOptions which allows character-specific saving of Addon settings.
It also comes with a profile manager, so users can copy their settings from one character to another, and even create their own profiles and switch between them.

[quote][SIZE=6][COLOR=#ffcc33]About[/COLOR][/SIZE][/quote]

With the introduction of multiple characters per account it seemed reasonable to offer an option for addon authors to easily implement character-specific settings.

This is a minor inconvenience for general settings like layout, but very useful when it comes to saving character specific data like inventory items or custom lists.

Settings are saved per character by default to provide a reliable catchall solution, however feel free to adjust the settings saving behaviour according to your users needs.

By using the Profile Selector you can offer an easy way for your users to manage their character-specific settings, or even create new profiles.

[quote][SIZE=6][COLOR=#ffcc33]Usage[/COLOR][/SIZE][/quote]

Use just like [font=Courier New]lib_InterfaceOptions[/font].

All of the following settings are saved PER CHARACTER by default now:

[code]
AddCheckBox
AddSlider
AddTextInput
AddColorPicker
AddChoiceMenu
AddChoiceEntry
StartGroup
AddMovableFrame

[/code]
To explicitly save a setting for ALL characters, the syntax is:

[code]
InterfaceOptions.Add____(options, true);

[/code]
Manually saved data via [font=Courier New]Component.SaveSetting("name", values)[/font] are also PER CHARACTER by default.
As above, to explicitly make the setting the same for all character, use:

[code]
Component.SaveSetting("name", values, true)

[/code]
Reading a manual setting does not require any flags to specify it's origin, and is the same as before:

[code]
Component.GetSetting("perCharacterSetting")
Component.GetSetting("GlobalSetting")

[/code]
[quote][SIZE=5][COLOR=#ffe699]Profile Manager[/COLOR][/SIZE][/quote]

[font=Courier New]InterfaceOptions.AddProfileSelector()[/font] adds a new OptionGroup (no nesting!) that allows users to copy their settings from one character to another.
This MUST be called AFTER [font=Courier New]OnComponentLoad[/font] fired, or it will break.
To copy settings, simply select the alternate profile and hit the button.

The Profile Manager also allows creating new profiles, and freely switching or copying settings between them.

In addition, users are able to export their current settings profile to share or restore it as needed.

[quote][SIZE=6][COLOR=#ffcc33]Examples[/COLOR][/SIZE][/quote]

[code]
require "./lib/lib_InterfaceOptionsExtended";

[/code]
Interface Options are saved per character by default, no need to do anything different

[code]
InterfaceOptions.AddCheckBox({id="checkbox_per_character", label="checkbox_per_character", checkbox=true, default=true});

[/code]
If you want to explicitly save an option on a global basis so it is the same for every character, the syntax changes to: [font=Courier New]InterfaceOptions.Add[color=#ffcc33][/color][u][/u](optionsTable, true);[/font]

[code]
InterfaceOptions.AddCheckBox({id="checkbox_all_characters", label="checkbox_all_characters", checkbox=true, default=true}, true);

[/code]
The same changes apply to frames, they are saved per character by default, and global when you use [font=Courier New].AddMovableFrame(options, true)[/font]

[code]
InterfaceOptions.AddMovableFrame({
  frame = PCFRAME,
  label = "Per Character Frame",
  scalable = true,
});

InterfaceOptions.AddMovableFrame({
  frame = ACFRAME,
  label = "All Character Frame",
  scalable = true,
}, true);

[/code]
This also applies to manual settings saving - check ui_savedsettings file to compare.

[code]
Component.SaveSetting("perCharacterTime", System.GetLocalUnixTime())
Component.SaveSetting("allCharactersTime", System.GetLocalUnixTime(), true)

[/code]
When loading settings, both options are checked, no need to specify global behavior.

[code]
log("perCharacterTime " .. tostring(Component.GetSetting("perCharacterTime")))
log("allCharactersTime " .. tostring(Component.GetSetting("allCharactersTime")))

[/code]
To attach the Profile Selector, [font=Courier New]InterfaceOptions.AddProfileSelector()[/font] must be called AFTER [font=Courier New]OnComponentLoad[/font], or it will break!
Since the Profile Selector adds an option group, nesting it between [font=Courier New]InterfaceOptions.StartGroup()[/font] and [font=Courier New].StopGroup()[/font] is not possible!

[code]
function OnComponentLoad() 

  InterfaceOptions.SetCallbackFunc(function(id, val)  OnMessage({type=id, data=val})  end, ADDON.NAME)
  InterfaceOptions.AddProfileSelector();

end

[/code]
[quote][SIZE=6][COLOR=#ffcc33]Issue Tracking[/COLOR][/SIZE][/quote]

Please report any issues you might find [url=https://bitbucket.org/Jotaro__/lib_interfaceoptionsextended/issues]here[/url]

[quote][SIZE=6][COLOR=#ffcc33]Changelog[/COLOR][/SIZE][/quote]

[code]
1.0.0 - initial release

  # Features
  - Save all InterfaceOptions settings on a per-character basis by default
  - Option to save specific settings globally
  - Profile Manager to easily change and edit existing profiles,
      including custom ones.
  - Profiles can also be exported and imported
[/code]
