var gulp = require('gulp');
var prompt = require('gulp-prompt');
var gulpFilter = require('gulp-filter');
var file = require('gulp-file');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var zip = require('gulp-zip');
var vinylPaths = require('vinyl-paths');
var map = require('vinyl-map');
var tree = require('markdown-tree');
var del = require('del');
var fs = require('fs');
var _ = require('underscore');
var watch = require('gulp-watch');
var runSequence = require('run-sequence');

var mdbb = require('./modules/mdbb/mdbb.js');

var p = require('./package.json');

var root = p.gulpOptions.root; // without trailing slash
var releasePath = p.gulpOptions.releasePath; // where to seperately copy the zip file to
var keepBuildPath = p.gulpOptions.keepBuildPath; // keep or delete the build folder (unzipped, built files)
var markdownParser = p.gulpOptions.markdownParser; // node module file providing bbcode parsing instruction,
// should be placed in `./modules/mdbb/parsers/`
var debugMarkdown = false; // set to true if you want to save the json object created by the markdown task

/* *********************
*  Addon build process
*  ********************/

var tasks = {};

tasks.files = function(){
  var filter = gulpFilter([
    p.name+'.lua',
    p.name+'.xml',
    'lib/*.lua'
  ], {restore: true});
  
  var files = gulp.src([
    root + '/**/*.*'
  ])

  return files
    .pipe(filter)
      .pipe(replace(/~%author%~/g, p.author))
      .pipe(replace(/~%version%~/g, p.version))
      .pipe(replace(/~%description%~/g, p.description))
      .pipe(replace(/~%patch%~/g, p.patch))
      .pipe(replace(/~%debug%~/g, 'false'))
    .pipe(filter.restore)
    .pipe(gulp.dest('./build/' + p.name))
};

tasks.melder_ini = function() {
  var br = "\n";
  
  var str = "title=" + p.name + br +
            "author=" + p.author + br +
            "version=" + p.version + br +
            "patch=" + p.patch + br +
            "url=" + p.url + br +
            "destination=" + p.destination + br +
            "description=" + p.description;
    
  return file('melder_info.ini', str, { src: true }).pipe(gulp.dest('./build'));
};

tasks.melder_button = function(){
  var button = "[center][url=http://astrekassociation.com/melder.php?id=#######][img]http://goomichan.github.io/Meldii/images/OneClick.png[/img][/url]\n"+
               "[size=1][color=#161C1C][melder_info]version="+
               p.version + ";" + 
               "patch=" + 
               p.patch + ";" + 
               "dlurl=#######[/melder_info][/color][/size][/center]";
  return file('melder_button.txt', button, { src: true }).pipe(gulp.dest('./dist'));
};

tasks.zip = function () {
    return gulp.src('build/**/*.*')
        .pipe(zip(p.name + '_' + p.version + '.zip'))
        .pipe(gulp.dest('dist'));
};

tasks.zipLib = function () {
    return gulp.src('build/'+p.name+'/lib/**/*.*')
        .pipe(zip('InterfaceOptionsExtended_' + p.version + '.zip'))
        .pipe(gulp.dest('dist'));
};

tasks.clean_build = function (cb) {
  del([
    './build'
  ], cb);
};

tasks.clean_dist = function (cb) {
  del([
    './dist'
  ], cb);
};

tasks.release = function(){
  if(releasePath != false){
    var rel = gulp.src('dist/*.zip')
    return rel.pipe(gulp.dest(releasePath))
  }
  return true;
};

/* *********************
*  Markdown parsing
*  ********************/

tasks.markdown = function() {
  
  var files = gulp.src([
    './*.md'
  ])
  
  var options = {
      gfm: true,
      tables: true,
      breaks: true,
      pedantic: true,
      sanitize: true,
      smartLists: true,
    };
  
  var markdown = map(function(content, filename) {
    content = content.toString();
    var jsonmap = tree(content, options);
    converter = new mdbb({parser:markdownParser});
    return converter.convert(jsonmap);
  });
 
 var json = map(function(content, filename) {
    content = content.toString();
    var jsonmap = JSON.stringify(tree(content, options), null, 2);
    return jsonmap;
  });
  
  if(debugMarkdown){
    files
      .pipe(json)
      .pipe(rename(function (path) {
        path.extname = '.json';
      }))
      .pipe(gulp.dest('./dist'));
  }
  
  return files
    .pipe(gulp.dest('./build'))
    .pipe(markdown)
    .pipe(rename(function (path) {
      path.extname = '.txt';
    }))
    .pipe(gulp.dest('./dist'));
};

/* *********************
*  Initial setup
*  ********************/

tasks.setup = function(){
  
  fs.exists(root + '/Template.lua', function (exists) {
    if(exists){
      setup();
    }else{
      console.log('Addon already customized!')
    }
  });
  
  
  
};

wizard = {

  name : {
    type: 'input',
    name: 'name',
    message: 'Title:',
    default: 'Template'
  },
  author : {
    type: 'input',
    name: 'author',
    message: 'Author:',
    default: ''
  },
  description : {
    type: 'input',
    name: 'description',
    message: 'Description:',
    default: ''
  },
  version : {
    type: 'input',
    name: 'version',
    message: 'Version:',
    default: '1.0.0'
  },
  destination : {
    type: 'input',
    name: 'destination',
    message: 'Destination:',
    default: 'gui/components/MainUI/Addons/'
  },
  deleteGit : {
    type: 'confirm',
    name: 'deleteGit',
    message: 'Delete cloned git folder? (default: Y)',
    default: true
  }
}

function setup(){
  file('', '', { src: true })
  .pipe(prompt.prompt([
    wizard.name,
    wizard.author,
    wizard.description,
    wizard.version,
    wizard.destination,
    wizard.deleteGit
  ], function(res){
    validateInput(res);
  }));
}

function validateInput(res){

  var askAgain = [];

  for(var k in res){
    if(res.hasOwnProperty(k) && isValid[k] && isValid[k](res[k]) == false){
      askAgain.push(wizard[k]);
    }else{
      p[k] = res[k];
    }
  }

  if(askAgain.length > 0) {
    console.log("\nSome entries were invalid, please provide them again:");
    file('', '', {src: true})
        .pipe(prompt.prompt(askAgain, function (res) {
          validateInput(res);
        }));
  }else{
    delete p.deleteGit;
    setupPackage();
    setupFiles();
    if(res.deleteGit){
      deleteGit();
    }
  }

}

var isValid = {
  version : function(version){
    var re = /^(?:\d{1,2}\.){2}\d{1,2}$/gm;
    return (re.exec(version) != null);
  }
}

function setupPackage(){
  return file('package.json', JSON.stringify(p, null, 2), { src: true })
        .pipe(gulp.dest('./'));
}

function setupFiles(){
  var files = gulp.src([
      root + '/Template.lua',
      root + '/Template.xml'
  ])
  return files
    .pipe(replace(/~%name%~/g, p.name))
    .pipe(vinylPaths(del))
    .pipe(rename(function (path) {
      path.basename = p.name;
    }))
    .pipe(gulp.dest(root))
}

function deleteGit(cb){
  del([
    './.git'
  ], cb);
}

/* *********************
*  Defining Task Dependencies
*  ********************/

gulp.task('files', tasks.files);
gulp.task('melder_ini', tasks.melder_ini);
gulp.task('melder_button', tasks.melder_button);
gulp.task('zip', tasks.zip);
gulp.task('zipLib', tasks.zipLib);
gulp.task('clean_build', tasks.clean_build);
gulp.task('clean_dist', tasks.clean_dist);
gulp.task('release', ['build'], tasks.release);
gulp.task('markdown', tasks.markdown);
gulp.task('setup', tasks.setup);

gulp.task('melder', ['melder_ini', 'melder_button']);
gulp.task('copy', ['melder', 'markdown', 'files']);
gulp.task('keep_build', function(){
  if(!keepBuildPath){
    runSequence('clean_build');
  }
});

gulp.task('build', function(callback) {
  runSequence(
         'clean_dist',
         'clean_build',
         'copy',
         'zip',
         'zipLib',
         'keep_build',
         callback);
});

// Auto build on change
gulp.task('watch', function () {
    watch(root + '/**/*.*', function () {
      runSequence('build');
    });
});

gulp.task('default', ['release']);


