module.exports = {
  
    Heading : {
    
      prefix : "[quote][SIZE=",
      option : function(depth) {
        var size = 6;
        var colors = [
          "ffcc33",
          "ffe699",
          "f0f0e1"
        ]
        var depth = (depth || 0);
        size = (depth <= 2) ? size : size - depth + 2;
        
        var color = colors[6-size] || "ffffff";
        
        return size + "][COLOR=#"+color+"]"
        
      },
      infix : "",
      suffix : "[/COLOR][/SIZE][/quote]\n\n"
      
    },
    
    img : {
      
      prefix : "[img]",
      option : null,
      infix : "$2",
      suffix : "[/img]"
      
    },
    
    url : {
      
      prefix : "[url=",
      option : null,
      infix : "$2]$1",
      suffix : "[/url]"
      
    },
    
    backticks : {
      
      prefix : "[font=Courier New]",
      option : null,
      infix : "$1",
      suffix : "[/font]"
      
    },
    
    bold : {
      
      prefix : "[b]",
      option : null,
      infix : "$1",
      suffix : "[/b]"
      
    },
    
    italics : {
      
      prefix : "[i]",
      option : null,
      infix : "$1",
      suffix : "[/i]"
      
    },

    colored : {

      prefix : "[color=#ffcc33]",
      option : null,
      infix : "$1",
      suffix : "[/color]"

    },

    underlined : {

      prefix : "[u]",
      option : null,
      infix : "$1",
      suffix : "[/u]"

    },

    paragraph : {
      
      prefix : "",
      option : function(depth) {
        return (depth > 0) ? "\n" : "\n\n"
      },
      infix : "",
      suffix : ""
      
    },

    list : {
      prefix : "[list]\n",
      option : function(depth){
        return (new Array(depth + 1)).join("  ");
      },
      infix : "[*]",
      suffix : "[/list]\n"
    },

    text : {

      prefix : "",
      option : null,
      infix : "",
      suffix : "\n"

    },
    
    blockquote : {
      
      prefix : "[quote]\n",
      option : null,
      infix : "",
      suffix : "[/quote]\n\n"
      
    },
    
    code : {
      
      prefix : "[code]\n",
      option : null,
      infix : "",
      suffix : "[/code]\n"
      
    },


  
  
}