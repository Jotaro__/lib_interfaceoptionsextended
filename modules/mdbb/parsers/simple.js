module.exports = {
  
    Heading : {
      
      prefix : "\n[size=",
      option : function(depth) {
        var size = 6;
        var depth = (depth || 0);
        return (depth <= 2) ? size : size - depth + 2;
      },
      infix : "]",
      suffix : "[/size]\n\n"
      
    },
    
    img : {
      
      prefix : "[img]",
      option : null,
      infix : "$2",
      suffix : "[/img]"
      
    },
    
    url : {
      
      prefix : "[url=",
      option : null,
      infix : "$2]$1",
      suffix : "[/url]"
      
    },
    
    backticks : {
      
      prefix : "[font=Courier New]",
      option : null,
      infix : "$1",
      suffix : "[/font]"
      
    },
    
    bold : {
      
      prefix : "[b]",
      option : null,
      infix : "$1",
      suffix : "[/b]"
      
    },
    
    italics : {
      
      prefix : "[i]",
      option : null,
      infix : "$1",
      suffix : "[/i]"
      
    },

    colored : {

      prefix : "[color=yellow]",
      option : null,
      infix : "$1",
      suffix : "[/color]"

    },

    underlined : {

      prefix : "[u]",
      option : null,
      infix : "$1",
      suffix : "[/u]"

    },

    paragraph : {
      
      prefix : "",
      option : function(depth) {
        return (depth > 0) ? "\n" : "\n\n"
      },
      infix : "",
      suffix : ""
      
    },

    list : {
        prefix : "[list]\n",
        option : function(depth){
          return (new Array(depth + 1)).join("  ");
        },
        infix : "[*]",
        suffix : "[/list]\n"
    },

    text : {
      
      prefix : "",
      option : null,
      infix : "",
      suffix : "\n"
      
    },
    
    blockquote : {
      
      prefix : "[quote]\n",
      option : null,
      infix : "",
      suffix : "[/quote]\n\n"
      
    },
    
    code : {
      
      prefix : "[code]\n",
      option : null,
      infix : "",
      suffix : "[/code]\n"
      
    },


  
  
}